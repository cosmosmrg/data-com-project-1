import binascii

def print_diff(b, x):
	print binascii.hexlify(bytearray(b[x-10:x-1])), ' ', binascii.hexlify(bytearray(b[x:x+10]))

def compare(input1,input2):
	with open(input1,'r') as file1:
		with open(input2,'r') as file2:
			b1, b2 = file1.read(), file2.read()
			print len(b1)
			for x in range(min(len(b2), len(b1))):
				if b1[x] != b2[x]:
					print "Found difference at byte : ", x
					print "First file: %s"%input1
					print b1[x-10:x-1], ' ', b1[x:x+10]
					print '-------------'
					print_diff(b1,x)
					print "Second file: %s"%input2		
					print b2[x-10:x-1], ' ', b2[x:x+10]
					print '-------------'
					print_diff(b2,x)
					return
			if len(b1) != len(b2):
				print "different length: %d, %d"%(len(b1), len(b2))
				return
	print 'Yeah!!! They are the same'


#compare('check.jpg', 'lionnoi.jpg')
compare('lionnoi.jpg','lionnoi1.jpg')
